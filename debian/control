Source: golang-github-labstack-echo
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Manas Kashyap <manaskashyaptech@gmail.com>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang
Build-Depends-Indep: golang-any,
                     golang-github-dgrijalva-jwt-go-dev,
                     golang-github-labstack-gommon-dev,
                     golang-github-stretchr-testify-dev,
                     golang-github-valyala-fasttemplate-dev,
                     golang-golang-x-crypto-dev
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-labstack-echo
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-labstack-echo.git
Homepage: https://github.com/labstack/echo
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/labstack/echo

Package: golang-github-labstack-echo-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-dgrijalva-jwt-go-dev,
         golang-github-labstack-gommon-dev,
         golang-github-stretchr-testify-dev,
         golang-github-valyala-fasttemplate-dev,
         golang-golang-x-crypto-dev
Description: High performance, minimalist Go web framework (library)
 Sourcegraph (https://sourcegraph.com/github.com/labstack/echo?badge)
 GoDoc (http://godoc.org/github.com/labstack/echo) Go Report Card
 (https://goreportcard.com/report/github.com/labstack/echo)
 Build Status (https://travis-ci.org/labstack/echo) Codecov
 (https://codecov.io/gh/labstack/echo) Join the chat at
 https://gitter.im/labstack/echo (https://gitter.im/labstack/echo) Forum
 (https://forum.labstack.com) Twitter (https://twitter.com/labstack)
 License (https://raw.githubusercontent.com/labstack/echo/master/LICENSE)
 Supported Go versions As of version 4.0.0, Echo is available as a Go
 module (https://github.com/golang/go/wiki/Modules).  Therefore a Go
 version capable of understanding /vN suffixed imports is required:
 • 1.9.7+• 1.10.3+• 1.11+ Any of these versions will allow you to
 import Echo as github.com/labstack/echo/v4 which is the recommended way
 of using Echo going forward.
 .
 For older versions, please use the latest v3 tag.  Feature Overview•
 Optimized HTTP router which smartly prioritize routes• Build robust
 and scalable RESTful APIs• Group APIs• Extensible middleware
 framework• Define middleware at root, group or route level• Data
 binding for JSON, XML and form payload• Handy functions to send
 variety of HTTP responses• Centralized HTTP error handling•
 Template rendering with any template engine• Define your format
 for the logger• Highly customizable• Automatic TLS via Let’s
 Encrypt• HTTP/2 supportBenchmarks Date: 2018/03/15 Source:
 https://github.com/vishr/web-framework-benchmark Lower is better!
 .
 .
 Guide (https://echo.labstack.com/guide)Example ```go package main
 .
 import (
   "net/http" "github.com/labstack/echo/v4"
   "github.com/labstack/echo/v4/middleware"
 )
 .
 func main() {
   // Echo instance e := echo.New()
 .
 // Middleware
   e.Use(middleware.Logger()) e.Use(middleware.Recover())
 .
 // Routes
   e.GET("/", hello)
 .
 // Start server
   e.Logger.Fatal(e.Start(":1323"))
 }
 .
 // Handler func hello(c echo.Context) error {
   return c.String(http.StatusOK, "Hello, World!")
 } ``` Help• Forum (https://forum.labstack.com)• Chat
 (https://gitter.im/labstack/echo)Contribute Use issues for everything •
 For a small change, just send a PR.• For bigger changes open an issue
 for discussion before sending a PR.• PR should have: • Test case•
 Documentation• Example (If it makes sense)• You can also contribute
 by: • Reporting issues• Suggesting new features or enhancements•
 Improve/fix documentationCredits• Vishal Rana (https://github.com/vishr)
 - Author• Nitin Rana (https://github.com/nr17) - Consultant•
 Contributors (https://github.com/labstack/echo/graphs/contributors)License
 MIT (https://github.com/labstack/echo/blob/master/LICENSE)
